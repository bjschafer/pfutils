import json
import re
import sys
from typing import List, Tuple, Dict

import requests

from bs4 import BeautifulSoup


def get_table(url: str):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find("table", attrs={"cellpadding": 5})
    return table


def get_header_index(all_headings: List, header_text: str) -> int:
    for i, header in enumerate(all_headings):
        if header_text in header.contents[0]:
            return i


def get_interesting_indexes(all_headings: List) -> Tuple[int, List[int]]:
    """
    :param all_headings: 
    :return: Tuple of (class level's table index, [table index of all spell levels])
    """
    ret_vals = []
    for i in range(0, 10):
        ret_vals.append(get_header_index(all_headings, f"{i}"))
    bad_index = get_header_index(all_headings, "domain spell")
    try:
        ret_vals.remove(bad_index)
    except:
        pass
    level_col = get_header_index(all_headings, "Level")
    return level_col, ret_vals


def has_cantrips(highest_spell_level: int) -> bool:
    if highest_spell_level == 9:
        return True
    return highest_spell_level % 2 == 0


def get_levels(table) -> Dict[int, Dict[int, int]]:
    ret = {}
    table_indexes = get_interesting_indexes(table.find_all("th"))
    all_rows: List = table.find_all("tr")
    for i, row in enumerate(all_rows[2:]):
        row_values = [x for x in row if x != "\n"]
        class_level = int(re.sub(r"\D", "", row_values[table_indexes[0]].contents[0]))
        if len(row_values) > 1:
            ret[class_level] = {}
            for j, spell_index in enumerate(table_indexes[1]):
                if spell_index:
                    spells_per_level = row_values[spell_index-1].contents[0]
                    if '+' in spells_per_level:
                        spells_per_level = spells_per_level.split('+')[0]
                    try:
                        ret[class_level][j] = int(spells_per_level)
                    except ValueError:
                        pass
    highest_spell_level = max(ret[20].keys())
    if not has_cantrips(highest_spell_level):
        for class_level, spell_levels in ret.items():
            ret[class_level] = {k+1: v for k, v in spell_levels.items()}
    return ret


def get_for_class(url: str):
    table = get_table(url)
    print(json.dumps({"SpellsPerLevel": get_levels(table)}, indent=4)[1:-1])


if __name__ == "__main__":
    # get_for_class("https://www.d20pfsrd.com/classes/hybrid-classes/investigator/")
    # get_for_class("https://www.d20pfsrd.com/classes/core-classes/cleric/")
    # get_for_class("https://www.d20pfsrd.com/classes/base-classes/magus/")
    get_for_class(sys.argv[1])
